import React, { useEffect } from "react";
import tailwindStyle from "~/styles/tailwind.css";
import globalStyle from "~/styles/global.css";

import type { MetaFunction } from "@remix-run/node"; // or cloudflare/deno
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useLoaderData,
  useLocation,
  useParams,
  useResolvedPath,
  useTransition,
} from "@remix-run/react";
import { getUserSession } from "./services/session.server";
import { json, LoaderFunction } from "@remix-run/node";
import Headers from "~/component/Header";
import remixI18n from "./i18n.server";
import { useTranslation } from "react-i18next";
import ErrorPage from "./component/ErrorPage";
import Footer from "./component/Footer";
import ModalStyle from "react-responsive-modal/styles.css";
import { Progress } from "flowbite-react";

export const loader: LoaderFunction = async ({ request }) => {
  const locale = await remixI18n.getLocale(request);
  const t = await remixI18n.getFixedT(request, "common");
  const title = t("headTitle");
  let user = await getUserSession(request);
  return json({ user, title, locale });
};

export const meta: MetaFunction = () => ({
  charset: "utf-8",
  viewport: "width=device-width,initial-scale=1",
  description: "annotation of text and discussion on budhist text",
});

export function links() {
  return [
    { rel: "stylesheet", href: tailwindStyle, as: "style" },
    { rel: "stylesheet", href: globalStyle, as: "style" },
    { rel: "stylesheet", href: ModalStyle, as: "style" },
  ];
}
export const handle = {
  // In the handle export, we could add a i18n key with namespaces our route
  // will need to load. This key can be a single string or an array of strings.
  i18n: ["common"],
};

function Document({ children, title }: any) {
  let data = useLoaderData();
  const transition = useTransition();
  let user = data?.user;
  let params = useParams();
  let location = useLocation();
  const { t, i18n } = useTranslation("common");
  let routeChanged =
    transition.state === "loading" &&
    transition.location.pathname.includes("/texts") &&
    !transition.location.state;
  return (
    <html lang={i18n.language}>
      <head>
        <Meta />
        <Links />

        <title>{t("title")}</title>
      </head>
      <body className="overflow-x-hidden">
        {!params.annotation && <Headers user={user} />}
        {routeChanged ? <Loading /> : children}

        {location.pathname === "/" && <Footer />}
        {process.env.NODE_ENV === "development" && <LiveReload />}

        <ScrollRestoration />
        <Scripts />
      </body>
    </html>
  );
}

function Loading() {
  const [percent, setPercent] = React.useState(0);
  let step = 0.5;
  let current_progress = 0;
  React.useEffect(() => {
    let interval = setInterval(function () {
      current_progress += step;
      let progress =
        Math.round((Math.atan(current_progress) / (Math.PI / 2)) * 100 * 1000) /
        1000;
      if (progress >= 100) {
        clearInterval(interval);
      } else if (progress >= 70) {
        step = 0.1;
      }
      setPercent(progress);
    }, 100);
    return () => {
      clearInterval(interval);
    };
  }, []);
  return <Progress progress={percent} size="sm" color="dark" />;
}

export function CatchBoundary() {
  return (
    <>
      <head>
        <Meta />
        <Links />
        <title>Error</title>
      </head>
      <ErrorPage />;
    </>
  );
}

export function ErrorBoundary({ error }: { error: Error }) {
  console.log(error);
  return (
    <Document title={"error ooh"}>
      <h1>App Error</h1>
      <pre color="red">{error.message}</pre>
      <p>
        try to go to here <a href="/">click</a>
      </p>
    </Document>
  );
}

function App() {
  return (
    <>
      <Document title={"Lopenling Application"}>
        <Outlet />
      </Document>
    </>
  );
}
export default App;

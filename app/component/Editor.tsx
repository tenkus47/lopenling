import { Form, useLoaderData, useTransition } from "@remix-run/react";
import Document from "@tiptap/extension-document";
import Highlight from "@tiptap/extension-highlight";
import TextStyle from "@tiptap/extension-text-style";
import Paragraph from "@tiptap/extension-paragraph";
import Bold from "@tiptap/extension-bold";
import Text from "@tiptap/extension-text";
import { BubbleMenu, EditorContent, useEditor } from "@tiptap/react";
import React, { useEffect } from "react";
// import SelectTextOnRender from "~/extension/selectionOnFirstRender";
import Post from "./Post";
import PostList from "./PostList";
import { useTranslation } from "react-i18next";
import { FontSize } from "~/tiptap-extension/fontSize";
// import SearchText from "./SearchText";
// import { searchMarks } from "~/extension/searchMarks";
import EditorSettings from "./EditorSettings";
import applyAnnotation from "~/tiptap-extension/applyMarks";
import { searchMarks } from "~/tiptap-extension/searchMarks";
import { Button } from "flowbite-react";
// import applyAnnotation from "~/extension/applyMarks";
const DEFAULT_FONTSIZE = 16;
function Editor() {
  const data = useLoaderData();
  const [searchLocation, setSearchLocation] = React.useState([]);
  const [postInfo, setPostInfo] = React.useState<null | {
    type: string;
    start: number;
    end: number;
    content: string;
  }>(null);
  const [selection, setSelection] = React.useState<null | {
    start: number;
    end: number;
    text: string;
  }>(null);
  const [openFilter, setOpenFilter] = React.useState<boolean>(false);
  const { t, ready } = useTranslation();
  let content = React.useMemo(() => {
    return data.text.content;
  }, []);
  const editor = useEditor(
    {
      extensions: [
        Document,
        Paragraph,
        Text,
        Bold,
        TextStyle,
        FontSize,
        searchMarks,
        applyAnnotation([], [], searchLocation),
        // SelectTextOnRender,
      ],
      content: content,
      editable: true,

      editorProps: {
        handleDOMEvents: {
          keypress: (value, event) => {
            event.preventDefault();
          },
          keyup: (value, event) => {
            event.preventDefault();
          },
          keydown: (value, event) => {
            if (![37, 38, 39, 40].includes(event.keyCode)) {
              event.preventDefault();
            }
          },
          textInput: (value, evt) => {
            evt.preventDefault();
          },
          drop: (value, e) => {
            e.preventDefault();
          },
          dragstart: (value, e) => {
            e.preventDefault();
          },
        },
      },
      onSelectionUpdate: ({ editor }) => {
        let from = editor.state.selection.from;
        let to = editor.state.selection.to;
        setPostInfo(null);
        setSelection({
          start: from,
          end: to,
          text: editor?.state.doc.textBetween(from, to, ""),
        });
      },
    },
    [searchLocation]
  );
  const handleBubbleClick = (type: string) => {
    if (selection)
      setPostInfo({
        type: type,
        start: selection.start,
        end: selection.end,
        content: selection.text,
      });
  };
  if (!ready) return <div>translation loading</div>;
  return (
    <div className="mt-5 flex w-full flex-col gap-5 md:flex-row">
      <div className="flex-1 px-5" style={{ maxHeight: "75vh" }}>
        <EditorSettings editor={editor} setSearchLocation={setSearchLocation} />
        <h1 className="mb-4 flex items-center justify-center text-lg font-bold text-gray-900">
          {data?.text?.name}
        </h1>

        <div className=" max-h-80 overflow-y-scroll md:max-h-full">
          {editor ? (
            <EditorContent
              editor={editor}
              className="editor"
              style={{
                transition: "all ease 0.3s",
              }}
            />
          ) : (
            <div>loading</div>
          )}
        </div>
        {editor && (
          <BubbleMenu
            className="BubbleMenu"
            editor={editor}
            tippyOptions={{ duration: 800, zIndex: 1 }}
          >
            <Button.Group outline={true}>
              <Button color="info" onClick={() => handleBubbleClick("comment")}>
                {t("Comment")}
              </Button>
              <Button
                color="info"
                onClick={() => handleBubbleClick("question")}
              >
                {t("Question")}
              </Button>
            </Button.Group>
          </BubbleMenu>
        )}
      </div>
      <div className="sm:w-full md:w-1/3">
        <div className="inline-flex w-full items-center justify-end">
          <div className="flex items-center justify-center space-x-2 rounded-lg border border-gray-200 px-3 py-2">
            <svg
              width="16"
              height="17"
              viewBox="0 0 16 17"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M2.3999 2.89998C2.3999 2.6878 2.48419 2.48432 2.63422 2.33429C2.78425 2.18426 2.98773 2.09998 3.1999 2.09998H12.7999C13.0121 2.09998 13.2156 2.18426 13.3656 2.33429C13.5156 2.48432 13.5999 2.6878 13.5999 2.89998V5.29998C13.5999 5.51213 13.5155 5.71558 13.3655 5.86558L9.5999 9.63117V12.5C9.59986 12.7121 9.51554 12.9156 9.3655 13.0656L7.7655 14.6656C7.65362 14.7774 7.51109 14.8536 7.35593 14.8844C7.20077 14.9153 7.03994 14.8995 6.89378 14.8389C6.74762 14.7784 6.62269 14.6759 6.53478 14.5443C6.44687 14.4128 6.39994 14.2582 6.3999 14.1V9.63117L2.6343 5.86558C2.48426 5.71558 2.39995 5.51213 2.3999 5.29998V2.89998Z"
                fill="#6B7280"
              />
            </svg>

            <button
              onClick={() => setOpenFilter((prev) => !prev)}
              className="text-sm font-medium leading-tight text-gray-500"
            >
              filter by
            </button>
          </div>
        </div>
        <Post postInfo={postInfo} ref={null} />

        <div
          style={{
            position: "relative",
            maxHeight: "80vh",
            overflowY: "scroll",
            overflowX: "hidden",
            paddingInline: 10,
          }}
        >
          <PostList
            editor={editor}
            setOpenFilter={setOpenFilter}
            openFilter={openFilter}
          />
        </div>
      </div>
    </div>
  );
}

export default Editor;

import { useFetcher, useLoaderData } from "@remix-run/react";
import { Editor } from "@tiptap/react";
import React, { useEffect } from "react";
type QuestionFormProps = {
  editor: Editor | null;
  postInfo: null | {
    type: string;
    start: number;
    end: number;
    content: string;
  };
};

const Posts = ({ postInfo }: QuestionFormProps, ref: any) => {
  const data = useLoaderData();
  const createPost = useFetcher();
  const inputRef = React.useRef(null);
  const [open, setOpen] = React.useState(false);
  const [body, setBody] = React.useState("");
  useEffect(() => {
    setOpen(!!postInfo);
  }, [postInfo]);
  function handleSubmit(e) {
    e.preventDefault();
    if (postInfo)
      createPost.submit(
        {
          start: postInfo.start,
          end: postInfo.end,
          selectedTextSegment: postInfo.content,
          textId: data?.text?.id,
          topic: data?.text?.name,
          body: body,
          type: postInfo.type,
        },
        {
          method: "post",
          action: "/api/post",
        }
      );
  }
  if (!postInfo) return null;
  if (!open) return null;
  if (createPost.data) setOpen(false);
  return (
    <section style={{ position: "sticky" }}>
      <div
        className="inline-flex items-start justify-start"
        style={{ width: 388, height: 20 }}
      >
        <p className="text-base font-medium leading-tight text-gray-900">
          New {postInfo.type}
        </p>
      </div>
      <createPost.Form
        ref={ref}
        className="flex flex-col gap-3"
        onSubmit={handleSubmit}
      >
        <div className="flex flex-col items-start justify-start rounded-lg border border-gray-300 bg-gray-50 px-5 py-3">
          <div className="h-full w-full flex-1">
            <div className="h-full flex-1 text-sm leading-tight text-gray-500">
              {data.user ? (
                <>
                  <textarea
                    autoFocus
                    name="body"
                    ref={inputRef}
                    onChange={(e) => setBody(e.target.value)}
                    className="h-12 w-full bg-gray-50 outline-0 "
                  ></textarea>
                </>
              ) : (
                <div style={{ color: "red" }}>You must login first</div>
              )}
            </div>
          </div>
        </div>
        <div className="flex justify-end gap-4">
          <div className="inline-flex-e items-center justify-center space-x-2 rounded-lg  bg-gray-200 px-3">
            <button
              className="text-xs font-medium leading-none text-gray-800"
              onClick={() => setOpen(false)}
            >
              cancel
            </button>
          </div>
          <div className="inline-flex items-center justify-center space-x-2 rounded-lg  bg-green-400 px-3">
            <button
              className="text-xs font-medium leading-none text-white"
              onClick={handleSubmit}
            >
              {createPost.state !== "idle" ? "posting" : "post"}
            </button>
          </div>
        </div>
      </createPost.Form>
    </section>
  );
};

export default React.forwardRef(Posts);

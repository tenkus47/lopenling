Welcome to Remix!
We are happy you're here!

Remix is a full stack web framework that lets you focus on the user interface and work back through web fundamentals to deliver a fast, slick, and resilient user experience that deploys to any Node.js server and even non-Node.js environments at the edge like Cloudflare Workers.

Want to know more? Read the Technical Explanation of Remix

This repository contains the Remix source code. This repo is a work in progress, so we appreciate your patience as we figure things out.

Documentation
For documentation about Remix, please visit our website.

Also, please join our community on Discord.

The documentation is automatically generated on each release from the files in the docs directory.

Contributing
If you're interested in contributing code and/or documentation, please see our guide to contributing.

Code of Conduct
Please see our code of conduct for any questions about the kind of community we are trying to build here and what to do if you need help with someone who is not acting professionally.
